const { PrismaClient } = require('./../src/generated/client');

const prisma = new PrismaClient();

const userData = [
    {
        username: 'llegit',
        firstname: 'Théo',
        lastname: 'Gillet',
        location: 'Saint Médard en Jalles, Gironde',
        country: 'France',
        email: 'theogillet.developpement@gmail.com',
        phoneNumber: "0679275900",
        password: '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
        avatar: null,
        weight: 78,
        height: 185,
        gender: 'MAN',
        age: 23
    },
    {
        username: 'cedricdoumbe',
        firstname: 'Cédric',
        lastname: 'Doumbé',
        location: 'Douala',
        country: 'Cameroun',
        email: 'cedricdoumbe@gmail.com',
        phoneNumber: "0634562090",
        password: '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
        avatar: 'https://lasueur.com/wp-content/uploads/2019/03/Cedric-Doumbe-Glory-64.png',
        weight: 66,
        height: 177,
        gender: 'MAN',
        age: 31
    },
    {
        username: "skyart",
        firstname: "Willy",
        lastname: "Dias",
        location: "Paris, île de france",
        country: "France",
        email: "skyart@gmail.com",
        phoneNumber: "0782167381",
        password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
        avatar: "https://static.wikia.nocookie.net/youtuberfrancais/images/5/5c/PQX1ZotF_400x400.jpg/revision/latest?cb=20180807163454&path-prefix=fr",
        weight: 70,
        height: 183,
        gender: "MAN",
        age: 31
    },
    {
        username: "chelxie",
        firstname: "Marie",
        lastname: "Celton",
        location: "Paris, île de france",
        country: "France",
        email: "chelxie@gmail.com",
        phoneNumber: "0689456701",
        password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
        avatar: "https://static.wikia.nocookie.net/youtuberfrancais/images/e/e4/960x1280_46b96a3bb2137c5ffe83d4e879435350.jpg/revision/latest?cb=20200919235848&path-prefix=fr",
        weight: 55,
        height: 160,
        gender: "WOMAN",
        age: 31
    },
];

const userProgrammeData = [
    {
        startDate: new Date(),
        endDate: new Date(),
        status: 'CREATED',
        idUser: 1,
        idProgramme: 1
    },
    {
        startDate: new Date(),
        endDate: new Date(),
        status: 'CREATED',
        idUser: 2,
        idProgramme: 3
    },
    {
        startDate: new Date(),
        endDate: new Date(),
        status: 'CREATED',
        idUser: 3,
        idProgramme: 2
    },
];

const sportData = [
    { name: "tennis", description: "Tennis is a racket sport that can be played individually against a single opponent (singles) or between two teams of two players each (doubles)." },
    { name: "running", description: "Running is a method of terrestrial locomotion allowing humans and other animals to move rapidly on foot. It is a type of gait characterized by an aerial phase in which all feet are above the ground." },
    { name: "powerlifting", description: "Powerlifting is a strength sport that consists of three attempts at maximal weight on three lifts: squat, bench press, and deadlift." },
    { name: "bodybuilding", description: "Bodybuilding is the use of progressive resistance exercise to control and develop one's musculature (muscle building) by muscle hypertrophy for aesthetic purposes." },
    { name: "fitness", description: "Fitness is the condition of being physically fit and healthy and involves attributes such as strength, endurance, flexibility, and overall body composition." }
];

const programmeData = [
    {
        title: "Intense running programme to prepare marathon",
        description: "This program focuses on intense running sessions to prepare for a marathon.",
        duration: 30,
        difficulty: "EXPERT",
        idSport: 2,
        trainings: [
            {
                title: "15 km training with variations",
                description: "This training aims to prepare softly but start with a good distance",
                stages: [
                    {
                        repetition: 1,
                        instructions: [
                            { title: "Warm-up", description: "Start with a 10-minute jog to warm up your muscles." },
                            { title: "Run", description: "Run at a comfortable pace for 10 kilometers." },
                            { title: "Cool-down", description: "Finish with a 5-minute walk to cool down." }
                        ],
                    },
                ],
            },
            {
                title: "Interval Training",
                description: "This training involves alternating periods of high-intensity effort with periods of rest or lower-intensity effort.",
                stages: [
                    {
                        repetition: 2,
                        instructions: [
                            { title: "Warm-up", description: "Start with a 5-minute jog to warm up your muscles." },
                            { title: "Sprint", description: "Sprint for 1 minute at maximum effort." },
                            { title: "Rest", description: "Rest for 2 minutes, walking or jogging slowly." }
                        ],
                    },
                ],
            },
            {
                title: "Hill Training",
                description: "This training involves running up and down hills to improve strength and endurance.",
                stages: [
                    {
                        repetition: 3,
                        instructions: [
                            { title: "Warm-up", description: "Start with a 10-minute jog to warm up your muscles." },
                            { title: "Hill Repeats", description: "Run uphill for 1 minute at maximum effort, then jog downhill to recover." },
                            { title: "Cool-down", description: "Finish with a 5-minute walk to cool down." }
                        ],
                    },
                ],
            }
        ]
    },
    {
        title: "Strength training for powerlifting competition",
        description: "This program focuses on strength training exercises to prepare for a powerlifting competition.",
        duration: 60,
        difficulty: "CONFIRMED",
        idSport: 3,
        trainings: [
            {
                title: "Squat and Deadlift Focus",
                description: "This training focuses on squat and deadlift variations for building strength.",
                stages: [
                    {
                        repetition: 1,
                        instructions: [
                            { title: "Warm-up", description: "Start with 5 minutes of dynamic stretching." },
                            { title: "Squat", description: "Perform 5 sets of 5 reps of back squats at 80% of your one-rep max." },
                            { title: "Deadlift", description: "Perform 5 sets of 5 reps of conventional deadlifts at 80% of your one-rep max." }
                        ],
                    },
                ],
            },
            {
                title: "Accessory Work",
                description: "This training includes accessory exercises to target specific muscle groups.",
                stages: [
                    {
                        repetition: 2,
                        instructions: [
                            { title: "Leg Press", description: "Perform 3 sets of 10 reps of leg presses." },
                            { title: "Pull-ups", description: "Perform 3 sets of 8 reps of pull-ups." },
                            { title: "Planks", description: "Hold a plank position for 3 sets of 1 minute." }
                        ],
                    },
                ],
            }
        ]
    },
    {
        title: "Bodybuilding transformation program",
        description: "This program is designed for bodybuilders looking to transform their physique.",
        duration: 90,
        difficulty: "BEGINNER",
        idSport: 4,
        trainings: [
            {
                title: "Upper Body Focus",
                description: "This training focuses on developing the muscles of the upper body.",
                stages: [
                    {
                        repetition: 1,
                        instructions: [
                            { title: "Warm-up", description: "Start with 10 minutes of light cardio and dynamic stretching." },
                            { title: "Chest", description: "Perform 4 sets of 8-10 reps of bench press." },
                            { title: "Back", description: "Perform 4 sets of 8-10 reps of bent-over rows." }
                        ],
                    },
                ],
            },
            {
                title: "Lower Body Focus",
                description: "This training focuses on developing the muscles of the lower body.",
                stages: [
                    {
                        repetition: 2,
                        instructions: [
                            { title: "Warm-up", description: "Start with 10 minutes of light cardio and dynamic stretching." },
                            { title: "Legs", description: "Perform 4 sets of 8-10 reps of squats." },
                            { title: "Hamstrings", description: "Perform 4 sets of 8-10 reps of Romanian deadlifts." }
                        ],
                    },
                ],
            }
        ]
    },
];

const allergyData = [
    { name: "Peanuts" },
    { name: "Shellfish" },
    { name: "Lactose" },
];

async function main() {
    console.log(`Start seeding ...`);
    for (const u of userData) {
        const user = await prisma.user.create({
            data: u,
        });
        console.log(`Created user with id: ${user.id}`);
    }
    for (const s of sportData) {
        const sport = await prisma.sport.create({
            data: s,
        });
        console.log(`Created sport with id: ${sport.id}`);
    }
    for (const p of programmeData) {
        const trainings = p.trainings;

        delete p.trainings;

        const programme = await prisma.programme.create({
            data: p,
        });

        console.log(`Created program with id: ${programme.id}`);

        for (const t of trainings) {
            const stages = t.stages;
            t.idProgramme = programme.id;

            delete t.stages;

            const training = await prisma.programmeTraining.create({
                data: t,
            });

            for (const st of stages) {
                const instructions = st.instructions;
                st.idProgrammeTraining = training.id;

                delete st.instructions;

                const stage = await prisma.programmeTrainingStage.create({
                    data: st,
                });

                for (const sti of instructions) {
                    sti.idProgrammeTrainingStage = stage.id;

                    await prisma.programmeTrainingStageInstruction.create({
                        data: sti,
                    });
                }
            }
        }
    }

    for (const up of userProgrammeData) {
        const userProgramme = await prisma.userProgramme.create({
            data: up,
        });
        console.log(`Created user program with id: ${userProgramme.id}`);
    }

    for (const allergy of allergyData) {
        const createdAllergy = await prisma.allergy.create({
            data: allergy,
        });
        console.log(`Created allergy with id: ${createdAllergy.id}`);
    }

    const user1 = await prisma.user.findUnique({ where: { id: 1 } });
    const user2 = await prisma.user.findUnique({ where: { id: 2 } });

    const peanutAllergy = await prisma.allergy.findUnique({ where: { id: 1 } });
    const shellfishAllergy = await prisma.allergy.findUnique({ where: { id: 2 } });

    await prisma.allergiesOnUsers.create({
        data: {
            userId: user1.id,
            allergyId: peanutAllergy.id,
        },
    });

    await prisma.allergiesOnUsers.create({
        data: {
            userId: user2.id,
            allergyId: shellfishAllergy.id,
        },
    });

    console.log(`Seeding finished.`);
}

main()
    .then(async () => {
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        console.error(e);
        await prisma.$disconnect();
        process.exit(1);
    });