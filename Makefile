setup-db:
	docker compose down -v
	docker compose up --build -d
	npx prisma generate
	npx prisma migrate dev --name init
	npx prisma db seed
