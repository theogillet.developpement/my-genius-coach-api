const express = require('express');
const { httpStatusCodes } = require('./utils/httpStatusCodes.js');
const { PrismaClient } = require('./generated/client');
const { checkIfValueIsNumber } = require('./utils/checkIfValueIsNumber.js');
const prisma = new PrismaClient();

const router = express.Router();

router.get('/all', async (req, res) => {
  const { itemsByPage, page } = req.query;

  if (!itemsByPage || !page) {
    const programmes = await prisma.programme.findMany();

    if (!programmes || programmes.length === 0) {
      return res.status(httpStatusCodes.NO_CONTENT).send();
    }

    return res.status(httpStatusCodes.OK).json(programmes);
  }
  if (
    !checkIfValueIsNumber(itemsByPage)
    || !checkIfValueIsNumber(page)
  ) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect query params, both must be number');
  }

  const pageNumber = parseInt(page);
  const itemsByPageNumber = parseInt(itemsByPage);
  const skipNumber = pageNumber < 2 ? 0 : ((pageNumber - 1) * itemsByPageNumber);

  const programmes = await prisma.programme.findMany({
    skip: skipNumber,
    take: itemsByPageNumber,
  });

  let programmesPaginated = {};

  const totalUsers = await prisma.programme.count();
  const totalPage = Math.round(totalUsers / itemsByPage);

  programmesPaginated.results = programmes;
  programmesPaginated.totalEntries = totalUsers;
  programmesPaginated.currentPage = pageNumber;
  programmesPaginated.totalPages = totalPage;
  programmesPaginated.itemsByPage = itemsByPageNumber;
  programmesPaginated.previousPage = pageNumber > 1 ? pageNumber - 1 : pageNumber;
  programmesPaginated.nextPage = pageNumber < totalPage ? pageNumber + 1 : pageNumber;

  if (!programmes || programmes.length === 0) {
    return res.status(httpStatusCodes.NO_CONTENT).send();
  }

  return res.status(httpStatusCodes.OK).json(programmesPaginated);
});

router.post('', async (req, res) => {
  const programme = req.body;

  if (
    !programme.title
    || !programme.description
    || !programme.duration
    || !programme.difficulty
    || !programme.idUser
    || !programme.idSport
  ) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Missing body fields');
  }

  const userExists = await prisma.user.findUnique({
    where: {
      id: programme.idUser,
    }
  });

  if (!userExists || userExists.length === 0) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('User does not exist');
  }

  const sportExists = await prisma.sport.findUnique({
    where: {
      id: programme.idSport,
    }
  });

  if (!sportExists || sportExists.length === 0) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Sport does not exist');
  }

  try {
    const existingProgramme = await prisma.programme.findFirst({
      where: {
        title: {
          contains: programme.name,
          mode: 'insensitive',
        },
      },
    });

    if (existingProgramme) {
      return res.status(httpStatusCodes.BAD_REQUEST).send('Programme already exists');
    }

    await prisma.programme.create({ data: programme });
  } catch (error) {
    console.error(error);
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send();
  }

  return res.status(httpStatusCodes.CREATED).send();
});

router.get('/:programmeId', async (req, res) => {
  const { programmeId } = req.params;

  if (!checkIfValueIsNumber(programmeId)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  const programme = await prisma.programme.findUnique({
    where: {
      id: parseInt(programmeId)
    }
  });

  if (!programme || programme.length === 0) {
    return res.status(httpStatusCodes.NOT_FOUND).send();
  }

  return res.status(httpStatusCodes.OK).json(programme);
});

router.patch('/:programmeId', async (req, res) => {
  const { programmeId } = req.params;
  const programme = req.body;

  if (!checkIfValueIsNumber(programmeId)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  const programmeInDb = await prisma.programme.findUnique({
    where: {
      id: parseInt(programmeId),
    }
  });

  if (!programmeInDb || programmeInDb.length === 0) {
    return res.status(httpStatusCodes.NOT_FOUND).send('Could not find the programme to update');
  }

  try {
    await prisma.programme.update({
      where: {
        id: parseInt(programmeId),
      },
      data: programme,
    });
  } catch (error) {
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
  }

  return res.status(httpStatusCodes.NO_CONTENT).send();
});

router.delete('/:programmeId', async (req, res) => {
  const { programmeId } = req.params;

  if (!checkIfValueIsNumber(programmeId)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  try {
    await prisma.programme.delete({
      where: {
        id: parseInt(programmeId),
      }
    });
  } catch (error) {
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
  }

  return res.status(httpStatusCodes.NO_CONTENT).send();
});

module.exports = router;
