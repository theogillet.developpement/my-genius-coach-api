module.exports.addDays = (days, date) => {
  date.setDate(date.getDate() + days);
  return date;
}
