module.exports.checkIfValueIsDate = (dateString) => {
  const date = new Date(dateString);
  return !isNaN(date.getTime());
}
