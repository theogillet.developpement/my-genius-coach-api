const express = require('express');
const { httpStatusCodes } = require('./utils/httpStatusCodes.js');
const { PrismaClient } = require('./generated/client');
const { checkIfValueIsNumber } = require('./utils/checkIfValueIsNumber.js');
const prisma = new PrismaClient();

const router = express.Router();

router.get('/all', async (req, res) => {
    const { itemsByPage, page } = req.query;

    const itemsByPageNumber = itemsByPage && checkIfValueIsNumber(itemsByPage) ? parseInt(itemsByPage) : 10;
    const pageNumber = page && checkIfValueIsNumber(page) ? parseInt(page) : 1;

    if (!checkIfValueIsNumber(itemsByPageNumber) || !checkIfValueIsNumber(pageNumber)) {
        return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect query params, both must be number');
    }

    const skipNumber = pageNumber < 2 ? 0 : ((pageNumber - 1) * itemsByPageNumber);

    const users = await prisma.user.findMany({
        skip: skipNumber,
        take: itemsByPageNumber,
        include: {
            AllergiesOnUsers: {
                include: {
                    allergy: true
                }
            }
        }
    });

    const totalUsers = await prisma.user.count();
    const totalPages = Math.ceil(totalUsers / itemsByPageNumber);

    let usersPaginated = {
        results: users.map(user => {
            const { AllergiesOnUsers, ...userWithoutAllergies } = user;
            return {
                ...userWithoutAllergies,
                allergies: AllergiesOnUsers.map(aou => aou.allergy.name)
            };
        }),
        totalEntries: totalUsers,
        currentPage: pageNumber,
        totalPages: totalPages,
        itemsByPage: itemsByPageNumber,
        previousPage: pageNumber > 1 ? pageNumber - 1 : pageNumber,
        nextPage: pageNumber < totalPages ? pageNumber + 1 : pageNumber
    };

    if (users.length === 0) {
        return res.status(httpStatusCodes.NO_CONTENT).send();
    }

    return res.status(httpStatusCodes.OK).json(usersPaginated);
});

router.post('', async (req, res) => {
    const user = req.body;

    if (
        !user.username
        || !user.firstname
        || !user.lastname
        || !user.location
        || !user.country
        || !user.email
        || !user.phoneNumber
        || !user.password
        || !user.avatar
        || !user.weight
        || !user.height
        || !user.gender
        || !user.age
    ) {
        return res.status(httpStatusCodes.BAD_REQUEST).send('Missing body fields');
    }

    try {
        const existingUser = await prisma.user.findFirst({
            where: {
                OR: [
                    { email: user.email },
                    { username: user.username },
                    { phoneNumber: user.phoneNumber },
                ],
            },
        });

        if (existingUser) {
            return res.status(httpStatusCodes.BAD_REQUEST).send('User already exist with this email, username or phone number');
        }

        await prisma.user.create({ data: user });
    } catch (error) {
        console.error(error);
        return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send();
    }

    return res.status(httpStatusCodes.CREATED).send();
});

router.get('/:userId', async (req, res) => {
    const { userId } = req.params;

    if (!checkIfValueIsNumber(userId)) {
        return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
    }

    const user = await prisma.user.findUnique({
        where: {
            id: parseInt(userId)
        }
    });

    if (!user || user.length === 0) {
        return res.status(httpStatusCodes.NOT_FOUND).send();
    }

    return res.status(httpStatusCodes.OK).json(user);
});

router.patch('/:userId', async (req, res) => {
    const { userId } = req.params;
    const user = req.body;

    if (!checkIfValueIsNumber(userId)) {
        return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
    }

    const userInDb = await prisma.user.findUnique({
        where: {
            id: parseInt(userId),
        }
    });

    if (!userInDb || userInDb.length === 0) {
        return res.status(httpStatusCodes.NOT_FOUND).send('Could not find the user to update');
    }

    try {
        await prisma.user.update({
            where: {
                id: parseInt(userId),
            },
            data: user,
        });
    } catch (error) {
        return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }

    return res.status(httpStatusCodes.NO_CONTENT).send();
});

router.delete('/:userId', async (req, res) => {
    const { userId } = req.params;

    if (!checkIfValueIsNumber(userId)) {
        return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
    }

    try {
        await prisma.user.delete({
            where: {
                id: parseInt(userId),
            }
        });
    } catch (error) {
        return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }

    return res.status(httpStatusCodes.NO_CONTENT).send();
});

module.exports = router;
