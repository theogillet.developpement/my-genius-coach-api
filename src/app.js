const express = require("express");
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const cors = require('cors');

const appPackage = require('./../package.json');

const router = (global.router = (express.Router()));
const userRouter = require('./user.js');
const authenticationRouter = require('./authentication.js');
const sportRouter = require('./sport.js');
const programmeRouter = require('./programme.js');
const userProgrammeRouter = require('./userProgramme.js');

const app = express();
app.use(express.json());
app.use(cors());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    next();
});

app.use(`/api/v${(appPackage.version).substring(0, 1)}`, router);

/**
 * @swagger
 * tags:
 *   name: User
 *   description: Every endpoints concerning the user
 * /user/all:
 *   get:
 *     summary: Fetch all the users
 *     tags: [User]
 *     responses:
 *       200:
 *         description: All users fetched
 *       204:
 *         description: No content fetched
 *       500:
 *         description: Internal server error
 * /user:
 *   post:
 *     summary: Create a user
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               firstname:
 *                 type: string
 *               lastname:
 *                 type: string
 *               location:
 *                 type: string
 *               country:
 *                 type: string
 *               email:
 *                 type: string
 *               phoneNumber:
 *                 type: string
 *               password:
 *                 type: string
 *               avatar:
 *                 type: string  
 *               weight:
 *                 type: number
 *               height:
 *                 type: number
 *               gender:
 *                 type: string
 *               age:
 *                 type: number
 *     responses:
 *       201:
 *         description: User created
 *       400:
 *         description: Missing correct body
 *       500:
 *         description: Internal server error
 * /user/{userId}:
 *   get:
 *     summary: Get a user by ID
 *     tags: [User]
 *     parameters:
 *       - in: path
 *         name: userId
 *         schema:
 *           type: integer
 *           required: true
 *         description: Numeric ID of the user to get
 *     responses:
 *       200:
 *         description: User fetched
 *       404:
 *         description: No user found with this id
 *       500:
 *         description: Internal server error
 *   patch:
 *     summary: Update a user by ID
 *     tags: [User]
 *     parameters:
 *       - in: path
 *         name: userId
 *         schema:
 *           type: integer
 *         required: true
 *         description: Numeric ID of the user to get
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *                 required: false
 *               firstname:
 *                 type: string
 *                 required: false
 *               lastname:
 *                 type: string
 *                 required: false
 *               location:
 *                 type: string
 *                 required: false
 *               country:
 *                 type: string
 *                 required: false
 *               email:
 *                 type: string
 *                 required: false
 *               password:
 *                 type: string
 *                 required: false
 *               avatar:
 *                 type: string
 *                 required: false
 *               weight:
 *                 type: number
 *                 required: false
 *               height:
 *                 type: number
 *                 required: false
 *               gender:
 *                 type: string
 *                 required: false
 *               age:
 *                 type: number
 *                 required: false
 *     responses:
 *       204:
 *         description: User updated
 *       404:
 *         description: No user found with this id
 *       500:
 *         description: Internal server error
 *   delete:
 *     summary: Delete a user by ID
 *     tags: [User]
 *     parameters:
 *       - in: path
 *         name: userId
 *         schema:
 *           type: integer
 *           required: true
 *         description: Numeric ID of the user to get
 *     responses:
 *       204:
 *         description: User deleted
 *       404:
 *         description: No user found with this id
 *       500:
 *         description: Internal server error
*/
router.use('/user', userRouter);

/**
 * @swagger
 * tags:
 *   name: Authentication
 *   description: Every endpoints concerning authentication
 * /login:
 *   post:
 *     summary: User login with username or email
 *     tags: [Authentication]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               username:
 *                 type: string
 *               password:
 *                 type: string  
 *     responses:
 *       200:
 *         description: User logged in
 *       400:
 *         description: Missing required fields
 *       401:
 *         description: Invalid credentials
 *       500:
 *         description: Internal server error
 * /register:
 *   post:
 *     summary: Create user account
 *     body:
 *     tags: [Authentication]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               firstname:
 *                 type: string
 *               lastname:
 *                 type: string
 *               location:
 *                 type: string
 *               country:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *               avatar:
 *                 type: string  
 *               weight:
 *                 type: number
 *               height:
 *                 type: number
 *               gender:
 *                 type: string
 *               age:
 *                 type: number  
 *     responses:
 *       200:
 *         description: User created
 *       400:
 *         description: Missing correct body
 *       500:
 *         description: Internal server error
 */
router.use(authenticationRouter);

/**
 * @swagger
 * tags:
 *   name: Sport
 *   description: Every endpoints concerning the sport
 * /sport/all:
 *   get:
 *     summary: Fetch all the sports
 *     tags: [Sport]
 *     responses:
 *       200:
 *         description: All sports fetched
 *       204:
 *         description: No content fetched
 *       500:
 *         description: Internal server error
 * /sport:
 *   post:
 *     summary: Create a sport
 *     tags: [Sport]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *     responses:
 *       201:
 *         description: sport created
 *       400:
 *         description: Missing correct body
 *       500:
 *         description: Internal server error
 * /sport/{sportId}:
 *   get:
 *     summary: Get a sport by ID
 *     tags: [Sport]
 *     parameters:
 *       - in: path
 *         name: sportId
 *         schema:
 *           type: integer
 *           required: true
 *         description: Numeric ID of the sport to get
 *     responses:
 *       200:
 *         description: sport created
 *       404:
 *         description: No sport found with this id
 *       500:
 *         description: Internal server error
 *   patch:
 *     summary: Update a sport by ID
 *     tags: [Sport]
 *     parameters:
 *       - in: path
 *         name: sportId
 *         schema:
 *           type: integer
 *         required: true
 *         description: Numeric ID of the sport to get
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *     responses:
 *       200:
 *         description: sport updated
 *       404:
 *         description: No sport found with this id
 *       500:
 *         description: Internal server error
 *   delete:
 *     summary: Delete a sport by ID
 *     tags: [Sport]
 *     parameters:
 *       - in: path
 *         name: sportId
 *         schema:
 *           type: integer
 *           required: true
 *         description: Numeric ID of the sport to get
 *     responses:
 *       204:
 *         description: Sport deleted
 *       404:
 *         description: No sport found with this id
 *       500:
 *         description: Internal server error
*/
router.use('/sport', sportRouter);

/**
 * @swagger
 * tags:
 *   name: Programme
 *   description: Every endpoints concerning the programme
 * /programme/all:
 *   get:
 *     summary: Fetch all the programmes
 *     tags: [Programme]
 *     responses:
 *       200:
 *         description: All programmes fetched
 *       204:
 *         description: No content fetched
 *       500:
 *         description: Internal server error
 * /programme:
 *   post:
 *     summary: Create a programme
 *     tags: [Programme]
 *     requestBody:
 *       required: true
 *     responses:
 *       201:
 *         description: programme created
 *       400:
 *         description: Missing correct body
 *       500:
 *         description: Internal server error
 * /programme/{programmeId}:
 *   get:
 *     summary: Get a programme by ID
 *     tags: [Programme]
 *     parameters:
 *       - in: path
 *         name: programmeId
 *         schema:
 *           type: integer
 *           required: true
 *         description: Numeric ID of the programme to get
 *     responses:
 *       200:
 *         description: programme created
 *       404:
 *         description: No programme found with this id
 *       500:
 *         description: Internal server error
 *   patch:
 *     summary: Update a programme by ID
 *     tags: [Programme]
 *     parameters:
 *       - in: path
 *         name: programmeId
 *         schema:
 *           type: integer
 *         required: true
 *         description: Numeric ID of the programme to get
 *     requestBody:
 *       required: true
 *     responses:
 *       200:
 *         description: programme updated
 *       404:
 *         description: No programme found with this id
 *       500:
 *         description: Internal server error
*/
router.use('/programme', programmeRouter);

/**
 * @swagger
 * tags:
 *   name: User Programme
 *   description: Every endpoints concerning the user programme
 * /user-programme/{id}/all:
 *   get:
 *     summary: Fetch all the programmes from a user
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *           required: true
 *         description: Numeric ID of the user programme to get
 *     tags: [User Programme]
 *     responses:
 *       200:
 *         description: All user programmes fetched
 *       204:
 *         description: No content fetched
 *       500:
 *         description: Internal server error
 * /user-programme:
 *   post:
 *     summary: Create a user programme
 *     tags: [User Programme]
 *     requestBody:
 *       required: true
 *     responses:
 *       201:
 *         description: user programme created
 *       400:
 *         description: Missing correct body
 *       500:
 *         description: Internal server error
 * /user-programme/{id}:
 *   get:
 *     summary: Get a user programme by ID
 *     tags: [User Programme]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *           required: true
 *         description: Numeric ID of the programme to get
 *     responses:
 *       200:
 *         description: Fetched user programme
 *       404:
 *         description: No user programme found with this id
 *       500:
 *         description: Internal server error
 *   patch:
 *     summary: Update a user programme by ID
 *     tags: [User Programme]
 *     parameters:
 *       - in: path
 *         name: programmeId
 *         schema:
 *           type: integer
 *         required: true
 *         description: Numeric ID of the programme to patch
 *     requestBody:
 *       required: true
 *     responses:
 *       204:
 *         description: user programme updated
 *       404:
 *         description: No user programme found with this id
 *       500:
 *         description: Internal server error
 *   delete:
 *     summary: Delete a user programme by ID
 *     tags: [User Programme]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *         required: true
 *         description: Numeric ID of the user programme to delete
 *     requestBody:
 *       required: true
 *     responses:
 *       204:
 *         description: user programme deleted
 *       404:
 *         description: No user programme found with this id
 *       500:
 *         description: Internal server error
*/
router.use('/user-programme', userProgrammeRouter);

const options = {
    definition: {
        openapi: "3.1.0",
        basePath: "/api/v1",
        info: {
            title: "My Genius Coach Api with Swagger",
            version: "0.1.0",
            description:
                "Api made with Node.js Express and Prisma Orm, to be used by our mobile app My Genius Coach",
            license: {
                name: "MIT",
                url: "https://spdx.org/licenses/MIT.html",
            },
            contact: {
                name: "llegit",
                url: "https://gitlab.com/theogillet.developpement",
                email: "theogillet.developpement@gmail.com",
            },
        },
        servers: [
            {
                url: "http://localhost:3000/api/v1",
                description: "Base URL for API version 1",
            },
        ],
    },
    apis: ["./src/app.js"],
};

const specs = swaggerJsdoc(options);
app.use(
    "/documentation",
    swaggerUi.serve,
    swaggerUi.setup(specs),
);

module.exports = app;
