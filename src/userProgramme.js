const express = require('express');
const { httpStatusCodes } = require('./utils/httpStatusCodes.js');
const { PrismaClient } = require('./generated/client');
const { checkIfValueIsNumber } = require('./utils/checkIfValueIsNumber.js');
const { addDays } = require('./utils/addDays.js');
const { isDate } = require('util/types');
const { checkIfValueIsDate } = require('./utils/checkIfValueIsDate.js');
const prisma = new PrismaClient();

const router = express.Router();

router.get('/:id/all', async (req, res) => {
  const idUser = req.params.id;

  if (!checkIfValueIsNumber(idUser)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  const userProgrammes = await prisma.userProgramme.findMany({
    where: {
      idUser: parseInt(idUser),
    },
    include: {
      programme: true,
    }
  });

  if (!userProgrammes || userProgrammes.length === 0) {
    return res.status(httpStatusCodes.NO_CONTENT).send();
  }

  return res.status(httpStatusCodes.OK).json(userProgrammes);
});

router.post('', async (req, res) => {
  const { idUser, startDate, idProgramme } = req.body;

  if (!idUser || !startDate || !idProgramme) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Missing body fields');
  }

  if (
    !checkIfValueIsNumber(idUser) ||
    !checkIfValueIsNumber(idProgramme)
  ) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('The ids provided in body are not numbers.');
  }

  if (!checkIfValueIsDate(startDate)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect startDate format.');
  }

  try {
    const existingUser = await prisma.user.findFirst({
      where: { id: parseInt(idUser) },
    });

    if (!existingUser) {
      return res.status(httpStatusCodes.BAD_REQUEST).send('User does not exist');
    }

    const existingProgramme = await prisma.programme.findFirst({
      where: { id: parseInt(idProgramme) },
    });

    if (!existingProgramme) {
      return res.status(httpStatusCodes.BAD_REQUEST).send('Programme does not exist');
    }

    const existingUserProgramme = await prisma.userProgramme.findFirst({
      where: {
        idUser: parseInt(idUser),
        idProgramme: parseInt(idProgramme),
      },
    });

    if (existingUserProgramme) {
      return res.status(httpStatusCodes.BAD_REQUEST).send('This programme has already been assigned to this user');
    }

    const endDate = addDays(existingProgramme.duration, new Date(startDate));

    await prisma.userProgramme.create({
      data: {
        idUser: parseInt(idUser),
        idProgramme: parseInt(idProgramme),
        startDate: new Date(startDate),
        endDate: endDate,
        status: 'CREATED',
      },
    });

    return res.status(httpStatusCodes.CREATED).send();
  } catch (error) {
    console.error(error);
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send();
  }
});

router.get('/:id', async (req, res) => {
  const idProgramme = req.params.id;

  if (!checkIfValueIsNumber(idProgramme)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  const userProgramme = await prisma.userProgramme.findUnique({
    where: {
      id: parseInt(idProgramme)
    }
  });

  if (!userProgramme) {
    return res.status(httpStatusCodes.NOT_FOUND).send();
  }

  return res.status(httpStatusCodes.OK).json(userProgramme);
});

router.patch('/:id', async (req, res) => {
  const idUserProgramme = req.params.id;
  const { idUser, idProgramme, startDate, endDate, status } = req.body;

  if (!checkIfValueIsNumber(idUserProgramme)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  if (
    idUser && !checkIfValueIsNumber(idUser)
    || idProgramme && !checkIfValueIsNumber(idProgramme)
  ) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('The ids provided in body are not numbers.');
  }

  const existingUserProgramme = await prisma.userProgramme.findUnique({
    where: {
      id: parseInt(idUserProgramme),
    },
    include: {
      programme: true,
    }
  });

  if (!existingUserProgramme) {
    return res.status(httpStatusCodes.NOT_FOUND).send('Could not find the user programme to update');
  }

  const userProgramme = {
    startDate: startDate ?? existingUserProgramme.startDate,
    endDate: endDate ?? existingUserProgramme.endDate,
    status: status ?? existingUserProgramme.status,
    idUser: idUser ? parseInt(idUser) : existingUserProgramme.idUser,
    idProgramme: idProgramme ? parseInt(idProgramme) : existingUserProgramme.idProgramme,
  }

  try {
    await prisma.userProgramme.update({
      where: {
        id: parseInt(idUserProgramme),
      },
      data: userProgramme,
    });
  } catch (error) {
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
  }

  return res.status(httpStatusCodes.NO_CONTENT).send();
});

router.delete('/:id', async (req, res) => {
  const idUserProgramme = req.params.id;

  if (!checkIfValueIsNumber(idUserProgramme)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  try {
    await prisma.userProgramme.delete({
      where: {
        id: parseInt(idUserProgramme),
      }
    });
  } catch (error) {
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
  }

  return res.status(httpStatusCodes.NO_CONTENT).send();
});

module.exports = router;
