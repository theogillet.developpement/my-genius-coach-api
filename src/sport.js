const express = require('express');
const { httpStatusCodes } = require('./utils/httpStatusCodes.js');
const { PrismaClient } = require('./generated/client');
const { checkIfValueIsNumber } = require('./utils/checkIfValueIsNumber.js');
const { globals } = require('./utils/globals.js');
const prisma = new PrismaClient();

const router = express.Router();

router.get('/all', async (req, res) => {
  const sports = await prisma.sport.findMany();

  if (!sports || sports.length === 0) {
    return res.status(httpStatusCodes.NO_CONTENT).send();
  }

  return res.status(httpStatusCodes.OK).json(sports);
});

router.post('', async (req, res) => {
  const sport = req.body;

  if (!sport.name || !sport.description) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Missing body fields');
  }

  if ((sport.description.length) > globals.DESCRIPTION_MAX_LENGTH) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Description field too long');
  }

  sport.name = (sport.name).toString().toLowerCase();

  try {
    const existingSport = await prisma.sport.findFirst({
      where: {
        name: {
          contains: sport.name,
          mode: 'insensitive',
        },
      },
    });

    if (existingSport) {
      return res.status(httpStatusCodes.BAD_REQUEST).send('Sport already exists');
    }

    await prisma.sport.create({ data: sport });
  } catch (error) {
    console.error(error);
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send();
  }

  return res.status(httpStatusCodes.CREATED).send();
});

router.get('/:sportId', async (req, res) => {
  const { sportId } = req.params;

  if (!checkIfValueIsNumber(sportId)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  const sport = await prisma.sport.findUnique({
    where: {
      id: parseInt(sportId)
    }
  });

  if (!sport || sport.length === 0) {
    return res.status(httpStatusCodes.NOT_FOUND).send();
  }

  return res.status(httpStatusCodes.OK).json(sport);
});

router.patch('/:sportId', async (req, res) => {
  const { sportId } = req.params;
  const sport = req.body;

  if (!checkIfValueIsNumber(sportId)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  const sportInDb = await prisma.sport.findUnique({
    where: {
      id: parseInt(sportId),
    }
  });

  if (!sportInDb || sportInDb.length === 0) {
    return res.status(httpStatusCodes.NOT_FOUND).send('Could not find the sport to update');
  }

  try {
    await prisma.sport.update({
      where: {
        id: parseInt(sportId),
      },
      data: sport,
    });
  } catch (error) {
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
  }

  return res.status(httpStatusCodes.NO_CONTENT).send();
});

router.delete('/:sportId', async (req, res) => {
  const { sportId } = req.params;

  if (!checkIfValueIsNumber(sportId)) {
    return res.status(httpStatusCodes.BAD_REQUEST).send('Incorrect id format, it must be a number.');
  }

  try {
    await prisma.sport.delete({
      where: {
        id: parseInt(sportId),
      }
    });
  } catch (error) {
    return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).send(error);
  }

  return res.status(httpStatusCodes.NO_CONTENT).send();
});

module.exports = router;
