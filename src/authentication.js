const express = require('express');
const { httpStatusCodes } = require('./utils/httpStatusCodes.js');
const { PrismaClient } = require('./generated/client');
const prisma = new PrismaClient();

const router = express.Router();

router.post('/login', async (req, res) => {
  return res.status(httpStatusCodes.OK).send();
});

router.post('/register', async (req, res) => {
  return res.status(httpStatusCodes.OK).send();
});

module.exports = router;