# my-genius-coach-api

## Getting started

- Ask for the `.env` file and its values, it needs to look exactly like the `.env.dist`

### If you can use Makefile commands
- Run `make setup-db`
- Exec `npm run dev` to run the node.js server

### If not...
- Make sure you have `Docker` installed and `Node.js/npm`
- Exec `docker compose up --build`
- To setup & populate db, run `npx prisma migrate dev --name init`
- Run `npx prisma generate`
- Exec `npm run dev` to run the node.js server

## Api documentation

- After server is launched, if you go to http://localhost:3000/documentation, you will see our Swagger documentation to consume the API
