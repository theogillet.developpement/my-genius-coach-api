const app = require("./src/app.js");
require('dotenv').config();

const port = process.env.SERVER_PORT || 3000;

app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});
